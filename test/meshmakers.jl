println("Testing meshmakers.jl...")
module MeshMakersTests
sq1(x) = squeeze(x,1)
using Base.Test
using Ragged
using LMesh

####
# boxmeshes
###

# 1D
n = 113
xr = [-4.1, 10.5]
dx = (diff(xr)/(n-1))[1]
m1 = checkmesh(make_boxmesh(n, xr))
v = Vertices(m1)
c = Cells(m1)
@test_throws  ErrorException make_boxmesh(-10, xr)

@test ndims(m1)==(1,1)
@test eltype(m1)==(Int, Float64)
@test sizes(m1)==(n, n-1)
@test haskey(meshfns(m1)[0], :bmark)
@test length(v)==n
@test length(c)==n-1
@test coords(m1)==linspace(xr[1], xr[2], n)'
bm = meshfns(m1)[0][:bmark]
co = sq1(coords(m1)[:,bm.>0])
@test co==xr
co = diff(coords(m1)[1,:],2)
@test_approx_eq(maximum(co),dx)
@test_approx_eq(minimum(co),dx)

## 2D simplex
tests = ((113, 11, [-4.1, 10.5], [3.1, 11.1]),
         (4, 3, [0, 1], [10, 12]),
        (3, 26, [-pi/2, pi/2], [-pi/2, pi/2]))
for (nx, ny, xr, yr) in tests
    m2 = checkmesh(make_boxmesh(nx, ny, xr, yr))
    v = Vertices(m2)
    c = Cells(m2)
    @test_throws  ErrorException make_boxmesh(-10, ny, xr, yr)

    @test ndims(m2)==(2,2)
    @test eltype(m2)==(Int, Float64)
    @test sizes(m2)==(nx*ny, (nx-1)*(ny-1)*3 + nx + ny -2, (nx-1)*(ny-1)*2)
    @test haskey(meshfns(m2)[0], :bmark)
    @test haskey(meshfns(m2)[1], :bmark)
    @test length(v)==nx*ny
    @test length(c)==(nx-1)*(ny-1)*2
    #@test coords(m2)==linspace(xr[1], xr[2], n)'
    bm = meshfns(m2)[0][:bmark]
    co = coords(m2)[:,bm.>0]
    @test all((co[1,:].==xr[1]) | (co[1,:].==xr[2]) | (co[2,:].==yr[1]) | (co[2,:].==yr[2]))
    # test edge lengths and numbers
    dx = (diff(xr)/(nx-1))[1]
    dy = (diff(yr)/(ny-1))[1]
    dxy = sqrt(dx^2+dy^2)
    counts = [0,0,0]
    for e in Edges(m2)
        dist = norm(diff(coords(e),2))
        counts[indmin(abs([dx,dy,dxy].-dist))] += 1
        @test_approx_eq(1.0, 1.0+minimum(abs([dx,dy,dxy].-dist)))
    end    
    @test counts==[(nx-1)*ny, (ny-1)*nx, (ny-1)*(nx-1)]
end

## 2D quad
tests = ((113, 11, [-4.1, 10.5], [3.1, 11.1]),
         (4, 3, [0, 1], [10, 12]))

for (nx, ny, xr, yr) in tests
    m2 = checkmesh(make_boxmesh(nx, ny, xr, yr; meshtype=:quad))
    v = Vertices(m2)
    c = Cells(m2)

    @test ndims(m2)==(2,2)
    @test eltype(m2)==(Int, Float64)
    @test sizes(m2)==(nx*ny, (nx-1)*(ny) + (nx)*(ny-1), (nx-1)*(ny-1))
    @test haskey(meshfns(m2)[0], :bmark)
    @test haskey(meshfns(m2)[0], :bmark)
    @test length(v)==nx*ny
    @test length(c)==(nx-1)*(ny-1)
    bm = meshfns(m2)[0][:bmark]
    co = coords(m2)[:,bm.>0]
    @test all((co[1,:].==xr[1]) | (co[1,:].==xr[2]) | (co[2,:].==yr[1]) | (co[2,:].==yr[2]))
    # test edge lengths and numbers
    dx = (diff(xr)/(nx-1))[1]
    dy = (diff(yr)/(ny-1))[1]
    counts = [0,0]
    for e in Edges(m2)
        dist = norm(diff(coords(e),2))
        counts[indmin(abs([dx,dy].-dist))] += 1
        @test_approx_eq(1.0, 1.0+minimum(abs([dx,dy].-dist)))
    end    
    @test counts==[(nx-1)*ny, (ny-1)*nx]
end

## 3D simplex
#        (nx, ny, nz, xr, yr, zr) 
tests = ((13, 11, 9, [-4.1, 10.5], [3.1, 11.1], [100.1, 111.0]),
         (3,   4, 5, [0,2+1], [10,13+2], [100, 104+3]),
         (2,   2, 3, [0,2+1], [10,13+2], [100, 104+3]),
         (2,   2, 2, [0,2], [10,13], [100, 104]))

for (nx, ny, nz, xr, yr, zr) in tests
    m3 = checkmesh(make_boxmesh(nx, ny, nz, xr, yr, zr))
    v = Vertices(m3)
    c = Cells(m3)
    @test_throws  ErrorException make_boxmesh(-10, ny, nz, xr, yr, zr)

    @test ndims(m3)==(3,3)
    @test eltype(m3)==(Int, Float64)
    n_quads = (nx-1)*(ny-1)*(nz-1)
    n_facet_quads = (nx-0)*(ny-1)*(nz-1) + (nx-1)*(ny-0)*(nz-1) + (nx-1)*(ny-1)*(nz-0)
    n_quad_edges = (nx-0)*(ny-0)*(nz-1) + (nx-1)*(ny-0)*(nz-0) + (nx-0)*(ny-1)*(nz-0)
    sizes_ = (nx*ny*nz, n_quad_edges+n_facet_quads, n_facet_quads*2+4*n_quads , n_quads*5)
    @test sizes(m3)==sizes_
    @test haskey(meshfns(m3)[0], :bmark)
    @test haskey(meshfns(m3)[2], :bmark)
    @test length(v)==nx*ny*nz
    @test length(c)==n_quads*5

    bm = meshfns(m3)[0][:bmark]
    co = coords(m3)[:,bm.>0]
    @test all(  (co[1,:].==xr[1]) | (co[1,:].==xr[2]) 
              | (co[2,:].==yr[1]) | (co[2,:].==yr[2])
              | (co[3,:].==zr[1]) | (co[3,:].==zr[2]))
    # test edge lengths and numbers
    dx = (diff(xr)/(nx-1))[1]
    dy = (diff(yr)/(ny-1))[1]
    dz = (diff(zr)/(nz-1))[1]
    dxy = sqrt(dx^2+dy^2)
    dxz = sqrt(dx^2+dz^2)
    dyz = sqrt(dy^2+dz^2)
    counts = zeros(Int,6)
    refdists = [dx,dy,dz,dxy,dxz,dyz]
    for e in Edges(m3)
        dist = norm(diff(coords(e),2))
        counts[indmin(abs(refdists.-dist))] += 1
        @test_approx_eq(1.0, 1.0+minimum(abs(refdists.-dist)))
    end
    @test sum(counts)==sizes_[2]
    @test counts==[(nx-1)*ny*nz, (ny-1)*nx*nz, (nz-1)*nx*ny,
                   (nx-1)*(ny-1)*(nz-0), (nx-1)*(ny-0)*(nz-1), (nx-0)*(ny-1)*(nz-1)]
end


## 3D quad
#        (nx, ny, nz, xr, yr, zr) 
tests = ((13, 11, 9, [-4.1, 10.5], [3.1, 11.1], [100.1, 111.0]),
         (3,   4, 5, [0,2+1], [10,13+2], [100, 104+3]),
         (2,   2, 3, [0,2+1], [10,13+2], [100, 104+3]),
         (2,   2, 2, [0,2], [10,13], [100, 104]))

for (nx, ny, nz, xr, yr, zr) in tests
    m3 = checkmesh(make_boxmesh(nx, ny, nz, xr, yr, zr; meshtype=:quad))
    v = Vertices(m3)
    c = Cells(m3)

    @test ndims(m3)==(3,3)
    @test eltype(m3)==(Int, Float64)
    n_quads = (nx-1)*(ny-1)*(nz-1)
    n_facet_quads = (nx-0)*(ny-1)*(nz-1) + (nx-1)*(ny-0)*(nz-1) + (nx-1)*(ny-1)*(nz-0)
    n_quad_edges = (nx-0)*(ny-0)*(nz-1) + (nx-1)*(ny-0)*(nz-0) + (nx-0)*(ny-1)*(nz-0)
    sizes_ = (nx*ny*nz, n_quad_edges, n_facet_quads , n_quads)
    @test sizes(m3)==sizes_
    @test haskey(meshfns(m3)[0], :bmark)
    @test haskey(meshfns(m3)[2], :bmark)
    @test length(v)==nx*ny*nz
    @test length(c)==n_quads

    bm = meshfns(m3)[0][:bmark]
    co = coords(m3)[:,bm.>0]
    @test all(  (co[1,:].==xr[1]) | (co[1,:].==xr[2]) 
              | (co[2,:].==yr[1]) | (co[2,:].==yr[2])
              | (co[3,:].==zr[1]) | (co[3,:].==zr[2]))
    # test edge lengths and numbers
    dx = (diff(xr)/(nx-1))[1]
    dy = (diff(yr)/(ny-1))[1]
    dz = (diff(zr)/(nz-1))[1]
    counts = zeros(Int,3)
    refdists = [dx,dy,dz]
    for e in Edges(m3)
        dist = norm(diff(coords(e),2))
        counts[indmin(abs(refdists.-dist))] += 1
        @test_approx_eq(1.0, 1.0+minimum(abs(refdists.-dist)))
    end
    @test sum(counts)==sizes_[2]
    @test counts==[(nx-1)*ny*nz, (ny-1)*nx*nz, (nz-1)*nx*ny]
end


#######
# Mattri Matlab interface
#######
using MAT
mesh = mattri_load_mesh("../test_meshes/circlemesh.mat")
# this takes a bit long because matlab needs to start:
# mesh = LMesh.MeshMakers.mattri_makemesh([0 0;1 0;1 1.], [1,2,2], [2,2,2], 0.01);

println("all passed.")
end # module

