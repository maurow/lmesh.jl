println("Testing LMesh.jl...")

#module LMeshTests
using Base.Test
import Ragged
using LMesh
RA = Ragged.RaggedArray

# load meshes
require("../test_meshes/example_meshes.jl")
using MeshExamples
ME = MeshExamples

###
# Connectivity and Topology tests
###
ra = RA(ME.connect_0_0)
c = Connect(0,0,ra)
c0 = Connect(Int,0,0)

@test c.c == ra
@test size(c) == size(c.c)
@test size(c0) == (0,0)
@test c[1,3]==ra[1,3]
c[1,3] = -999
@test c[1,3]==-999
@test ndims(c)==2
@test tdims(c)==(0,0)
@test isempty(c)== false
@test isempty(c0)== true
@test maximum(c)==maximum(c.c)
@test minimum(c)==minimum(c.c)

@test_throws ErrorException Topology(0, [c,c].'.')

topo = Topology(3, Int)
@test all(isempty(topo))
@test size(topo)==(3,3)
@test ndims(topo)==2
@test topo[0,0]==Connect(Int,0,0)
@test any(isempty(topo))

## Mesh tests
m0 = Mesh(0,0)
@test_throws ErrorException Mesh(1,0)
co = rand(2,5)
m0 = Mesh(0,0,co) 
@test coords(m0)==co
@test tdims(m0)==ndims(m0)[1]
@test sdims(m0)==ndims(m0)[2]
@test size(m0)==(0,0)

# triangular mesh with
# 13 vertices
# 28 edges
# 16 cells
nums = [13,28,16]
meshes = make_meshes()
map(checkmesh, meshes)
mesh11, mesh11_, mesh12, trimesh, trimesh_, vormesh = meshes

## tidy up these tests a bit!

#####
# misc
######
trimesh[end,end]

vertices = Vertices(mesh11)
@test isa(vertices[2:3],Entities)
@test isa(vertices[2],Entity)


#####
# 1D
#####
@test ndims(mesh11)==(1,1)
@test ndims(mesh12)==(1,2)

for mesh in (mesh11, mesh12, mesh11_)
    @test sizes(mesh)==(ME.n_vert, ME.n_cells)
    @test all(mesh.topo[1,0].c .==ME.connect_1_0)
    # more checks once .== is implemented for ragged_array
    @test all(meshfns(mesh)[0][:bmark].==ME.bmark1d)
end
# mesh indexing
@test mesh11[0,0]==mesh11.topo[0,0]
@test mesh11[0,end]==mesh11.topo[0,1]
@test mesh11[end,1]==mesh11.topo[1,1]
@test mesh11[end,end]==mesh11.topo[1,1]
@test mesh11[end-1,end-1]==mesh11.topo[0,0]
@test mesh11[1]==mesh11.topo[1,0]
@test_throws ErrorException mesh11[end]

# global iterators
vertices = Vertices(mesh11)
edges = Edges(mesh11)
facets = Facets(mesh11)
cells = Cells(mesh11)

@test getmesh(vertices)===mesh11
@test tdim(vertices)==0
@test length(vertices)==ME.n_vert
@test tdim(edges)==1
@test length(edges)==ME.n_cells
@test tdim(facets)==0
@test length(facets)==ME.n_vert
@test tdim(cells)==1
@test length(cells)==ME.n_cells
@test coords(vertices)==ME.vertices1d
@test coords(vertices,1)==ME.vertices1d
@test_throws ErrorException coords(cells)

for (i,v) in enumerate(vertices)
    @test coords(v)==ME.vertices1d[:,i]
    @test coords(v,2)==[ME.vertices1d[:,i],0.0]
    @test con(v,0)==ME.connect_0_0[i]
    @test con0(v)==ME.connect_0_0[i]
    @test con(v,1)==ME.connect_0_1[i]
    @test meshfns(v,:bmark)==ME.bmark1d[i]
end
for (i,c) in enumerate(cells)
    @test all(con(c,0).==ME.connect_1_0[:,i])
    @test all(con(c,1).==ME.connect_1_1[i])
    @test all(coords(c).==ME.vertices1d[:,con(c,0)])
    tmp = ME.vertices1d[:,con(c,0)]
    @test all(coords(c,2).==[tmp; zeros(eltype(tmp), size(tmp)...)])
    for (ii,cc) = enumerate(incident0(c)) # iterates over incident vertices
        isequal(cc, vertices[con(c,0)[ii]])
    end
end
start_ = 2; stop = 4
for (i,v) in enumerate(vertices[start_:stop])
    @test all(coords(v).==ME.vertices1d[:,start_-1+i])
    @test all(con(v,0).==ME.connect_0_0[start_-1+i])
end


######
# 2D
#####
siz =[(13,28,16), (13,28,16), (42,57,16)]
for (ii, mesh) in enumerate((trimesh, trimesh_, vormesh))
    @test sizes(mesh)==siz[ii]
    @test ndims(mesh)==(2,2)
end

@test all(trimesh.topo[2,0].c .==ME.triconnect_2_0)
@test all(trimesh.topo[1,0].c .==ME.triconnect_1_0)
@test all(trimesh.topo[1,2].c .==ME.triconnect_1_2)
@test all(meshfns(trimesh)[0][:bmark].==ME.tribmark)
## check once .== is implemented for ragged_array
# @test all(trimesh.topo[1,2].c .==triconnect_0_1)
# @test all(trimesh.topo[2,0].c .==vorconnect_2_0)
@test all(meshfns(vormesh)[0][:bmark].==ME.vorbmark)

for td=0:ndims(trimesh)[1]
    @test sizes(trimesh, td)==nums[td+1]
end

# global iterators
vertices = Vertices(trimesh)
edges = Edges(trimesh)
facets = Facets(trimesh)
cells = Cells(trimesh)
@test isempty(meshfns(edges))
@test isempty(meshfns(facets))
@test isempty(meshfns(cells))


@test tdim(vertices)==0
@test length(vertices)==nums[1]
@test tdim(edges)==1
@test length(edges)==nums[2]
@test tdim(facets)==1
@test length(facets)==nums[2]
@test tdim(cells)==2
@test length(cells)==nums[3]

for (i,v) in enumerate(vertices)
    @test all(coords(v).==ME.trivertices[:,i])
    @test all(con(v,1).==ME.triconnect_0_1[i])
    @test meshfns(v,:bmark)==ME.tribmark[i]
end
for (i,e) in enumerate(edges)
    @test all(con(e,0).==ME.triconnect_1_0[:,i])
    @test all(con(e,2).==ME.triconnect_1_2[:,i])
    @test all(coords(e).==ME.trivertices[:,con(e,0)])
end
for (i,c) in enumerate(cells)
    @test all(con(c,0).==ME.triconnect_2_0[:,i])
    @test all(coords(c).==ME.trivertices[:,con(c,0)])
    @test all(coords(c,2).==ME.trivertices[:,con(c,0)])
    @test_throws BoundsError coords(c,1)
    tmp = ME.trivertices[:,con(c,0)]
    @test all(coords(c,4).==[tmp; zeros(eltype(tmp), size(tmp)...)])
end
start_ = 2; stop = 8
for (i,e) in enumerate(edges[start_:stop])
    @test all(con(e,0).==ME.triconnect_1_0[:,start_-1+i])
    @test all(con(e,2).==ME.triconnect_1_2[:,start_-1+i])
    @test all(coords(e).==ME.trivertices[:,con(e,0)])
end



println("all passed.")
##################################################
# missing features
##################################################

check_failing_tests = false

if check_failing_tests
    println("Checking tests for meshj.jl which are failing:")
    @test all(trimesh.topo[1,2].c .==ME.triconnect_0_1)
    @test isequal(vertices, Entities(trimesh,0))
    @test isequal(edges, Entities(trimesh,1))
    @test isequal(cells, Entities(trimesh,2))
end
#end # module
