test_type = length(ARGS) == 1 ? ARGS[1] : "ALL"

using Base.Test
using LMesh

# some setup
println("Running tests for meshj.jl...")
if test_type == "ALL" || test_type == "RAGGEDARRAY"
    include("lmesh.jl")
    include("meshmakers.jl")
end

# if test_type == "ALL" || test_type == "PERF"
#     x = Example.domath(3.0)
#     @time for run = 1:1000000
#         x = Example.domath(run)
#     end
# end
