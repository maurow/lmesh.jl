Julia mesh package
==================

Implements the type of mesh sugessted by
[Logg (2012)](http://arxiv.org/pdf/1205.3081.pdf).  Although
implementing the Sieve-mesh of
[Knepley & Karpeev (2009)](http://arxiv.org/abs/0908.4427) may be more
general.  In fact, Logg seems to struggle with meshes in 3D and higher
which are not simplices.

Other references:

- [DMPlex class in PETSc](http://www.mcs.anl.gov/petsc/petsc-dev/docs/manual.pdf)
- [DMPlex example](http://www.mcs.anl.gov/petsc/petsc-dev/src/ts/examples/tutorials/ex11.c.html)
- [ftp://140.221.6.23/pub/tech_reports/reports/P1295.pdf](ftp://140.221.6.23/pub/tech_reports/reports/P1295.pdf)
- [https://iospress.metapress.com/content/k5647wt076773465/resource-secured/?target=fulltext.pdf](https://iospress.metapress.com/content/k5647wt076773465/resource-secured/?target=fulltext.pdf)

Misc:

- [Julia sparse datastructure](http://docs.julialang.org/en/latest/manual/arrays/#compressed-sparse-column-csc-storage)

Installation
------------
```
Pkg.clone("git@bitbucket.org:maurow/ragged.jl.git", "Ragged")
Pkg.clone("git@bitbucket.org:maurow/lmesh.jl.git", "LMesh")
```
The explicit directory naming is necessary because bitbucket uses
lowercase directories.

Structured box meshes
---------------------

Structured simplex and quad meshed on a line, rectangle or cuboid can
be made with the the command `make_boxmesh` in
`src/meshmakers/boxmeshes.jl`.


Mattri interface
----------------

Currently the best way to generate general 2D triangular meshes is
through an interface to [mattri](https://bitbucket.org/maurow/mattri),
a Matlab wrapper around Triangle.  With that installed can do:

```julia
julia> using LMesh

julia> mesh = LMesh.MeshMakers.mattri_makemesh([0 0;1 0;1 1.], [1,2,2], [2,2,2], 0.01);
A MATLAB session is open successfully
>> Mesh with 55 nodes.
```

to create
![plot](https://bytebucket.org/maurow/mattri/raw/master/doc/examplemesh.png)

Related packages
----------------
- [JuliaGeometry](https://github.com/JuliaGeometry)
- [Meshes.jl](https://github.com/twadleigh/Meshes.jl)
- [Grid.jl](https://github.com/timholy/Grid.jl)

Mesh generators
---------------
- [distmesh looks well cool](http://persson.berkeley.edu/distmesh/)

Install
=======
Dependencies:

- [Ragged.jl](https://bitbucket.org/maurow/ragged.jl)
