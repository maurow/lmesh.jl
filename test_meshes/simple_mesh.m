% Produces a simple mesh in Matlab using https://bitbucket.org/maurow/mattri
boundary_xy = [0,0;
               1.1,0.1;
               1,1.1;
               0.1,0.9];  % make wonky as otherwise the Voronoi is not correct (degenerate)
bmark = [2, 3, 4, 5]
maxarea = 0.1;
[mesh, boundary_inds] = make_mesh(boundary_xy, bmark, bmark+10, maxarea);
figure
mesh_plot_tri(gca, mesh.tri)
figure
mesh_plot_dual(gca, mesh, 1)