module MeshExamples
using LMesh
import Ragged
export make_meshes
RA = Ragged.RaggedArray
########
## 1D
########
n_vert = 5
n_cells = n_vert -1
vertices1d = (linspace(0,3.5,n_vert))'  # make an array
connect_1_0 = [[1:n_vert-1]'; [2:n_vert]']
connect_0_1 = Vector{Int}[[1],[[i,i+1] for i=1:n_cells-1]...,[n_cells]]
# connect_0_0: all other vertices incident on the same cell
connect_0_0 = Vector{Int}[[2],[[i-1,i+1] for i=2:n_vert-1]...,[n_vert-1]]
# connect_1_1: all other cells sharing a vertex with current cell
connect_1_1 = Vector{Int}[[2],[[i-1,i+1] for i=2:n_cells-1]...,[n_cells-1]]
bmark1d = collect(1:n_vert)

vertices2d = [linspace(0,3.5,n_vert)'; 1.+linspace(0,3.5,n_vert)']

#######
## 2D
#######
# these were generated with Triangle via Mauro's matlab interface: simple_mesh.m

# triangular mesh with:
# 13 vertices
# 28 edges
# 16 cells

trivertices = [0 0
               0.5000 0
               0 0.5000
               0.2500 0.2500
               0.2500 0.7500
               0 1.0000
               0.7500 0.2500
               1.0000 0
               0.5000 0.5000
               0.5000 1.0000
               1.0000 0.5000
               0.7500 0.7500
               1.0000 1.0000]'
triconnect_2_0 = [11 12  9
                  3  4  9
                  9 10  5
                  9  5  3
                  9  4  2
                  2  7  9
                  9  7 11
                  9 12 10
                  8 11  7
                  2  8  7
                  6  3  5
                  10  6  5
                  13 10 12
                  11 13 12
                  1  2  4
                  3  1  4]'
triconnect_1_0 = [11 12
                  12  9
                  9 11
                  3  4
                  4  9
                  9  3
                  9 10
                  10  5
                  5  9
                  5  3
                  4  2
                  2  9
                  2  7
                  7  9
                  7 11
                  12 10
                  8 11
                  7  8
                  2  8
                  6  3
                  5  6
                  10  6
                  13 10
                  12 13
                  11 13
                  1  2
                  4  1
                  3  1]'
# this should be a ragged array with -1 removed:
triconnect_1_2 = [ 1 14
                  1  8
                  1  7
                  2 16
                  2  5
                  2  4
                  3  8
                  3 12
                  3  4
                  4 11
                  5 15
                  5  6
                  6 10
                  6  7
                  7  9
                  8 13
                  9 -1
                  9 10
                  10 -1
                  11 -1
                  11 12
                  12 -1
                  13 -1
                  13 14
                  14 -1
                  15 -1
                  15 16
                  16 -1]'
triconnect_0_1=Vector{Int}[[ 26, 27, 28],
                [ 12, 13, 19, 11, 26],
                [  4, 28, 6, 10, 20],
                [  5, 11, 27, 4],
                [  9, 10, 21, 8],
                [ 20, 21, 22],
                [ 14, 15, 18, 13],
                [ 17, 18, 19],
                [  3, 6, 7, 2, 5, 9, 12, 14],
                [  8, 22, 7, 16, 23],
                [  1, 25, 3, 15, 17],
                [  2, 16, 24, 1],
                [ 23, 24, 25]]

tribmark = [2
            2
            2
            0
            0
            5
            0
            3
            0
            2
            2
            0
            4]

# the dual voronoi mesh
vorvertices =[ 
    0.9042    0.7136
    0.8159    0.9560
    0.8379    0.4795
    0.1407    0.3617
    0.2355    0.5133
    0.4682    0.2407
    0.3604    0.8808
    0.8009    0.9143
    0.9267    0.3364
    0.5729    0.5614
    0.9129    0.3430
    0.6378    0.1760
    0.5217    0.7948
    0.1692    0.6883
    0.1152    0.1130
    0.3611    0.1696
    0.1991    0.1952
    0.4535    0.4490
    0.0138    0.1242
    0.1245    0.0113
         0         0
    0.9495    0.0863
    1.0749    0.3512
    1.1000    0.1000
    1.0124    0.9756
    0.7940    1.0542
    1.0000    1.1000
    0.3440    0.9542
    0.0776    0.6985
    0.1000    0.9000
    0.6485    0.0590
    0.3735    0.0340
    0.4979    0.0453
    0.0414    0.3727
    0.0552    0.4969
    0.5881    1.0085
    1.0373    0.7269
    1.0498    0.6025
    1.0249    0.8512
    0.7990    0.0726
    0.0276    0.2485
    0.2490    0.0226]'
# something is wrong with this:
vorconnect_2_2= Vector{Int}[[     3, 2],
     [1, 3, 5, 4],
     [6, 5, 1, 2],
     [7, 5, 8, 2],
     [3, 6,10, 7, 4, 2],
     [3, 5,10,11],
    [13,12, 4, 5,10, 8],
    [13, 9, 7, 4],
     [8,13],
     [6, 5,11,14,12, 7],
    [14,10, 6],
    [15,13, 7,14,10],
    [15,12, 7, 8, 9],
    [16,15,11,10,12],
    [13,12,16,14],
    [15,14]]
vorconnect_1_0=[
           1          37
           1           8
           1           3
           2          25
           2          26
           2           8
           3          11
           3          10
           4          34
           4          17
           4           5
           5          14
           5          18
           6          12
           6          18
           6          16
           7          28
           7          14
           7          13
           8          13
           9          11
           9          22
           9          23
          10          13
          10          18
          11          12
          12          31
          14          29
          15          19
          15          20
          15          17
          16          17
          16          32
          21          19
          21          20
          24          22
          24          23
          27          25
          27          26
          30          28
          30          29
          33          31
          33          32
          35          29
          35          34
          36          26
          36          28
          38          23
          38          37
          39          25
          39          37
          40          22
          40          31
          41          19
          41          34
          42          20
          42          32]'

vorbmark = [    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1
    1]

function make_meshes()
    # 1D 
    mesh11 = Mesh{1, 1, Int, Float64}(vertices1d)
    mesh11.topo[0,0] = Connect(0,0,RA(connect_0_0))
    mesh11.topo[0,1] = Connect(0,1,RA(connect_0_1))
    mesh11.topo[1,0] = Connect(1,0,connect_1_0)
    mesh11.topo[1,1] = Connect(1,1,RA(connect_1_1))
    mesh11.fns[0][:bmark] = bmark1d
    # not fully filled
    mesh11_ = Mesh{1, 1, Int, Float64}(vertices1d)
    mesh11_.topo[1,0] = Connect(1,0,connect_1_0)
    mesh11_.fns[0][:bmark] = bmark1d
    # 1D with vertices in 2d
    mesh12 = Mesh{1, 2, Int, Float64}(vertices2d)
    mesh12.topo[0,0] = Connect(0,0,RA(connect_0_0))
    mesh12.topo[0,1] = Connect(0,1,RA(connect_0_1))
    mesh12.topo[1,0] = Connect(1,0,connect_1_0)
    mesh12.topo[1,1] = Connect(1,1,RA(connect_1_1))
    mesh12.fns[0][:bmark] = bmark1d

    # 2D triangular
    trimesh = Mesh{2, 2, Int, Float64}(trivertices)
    ra_triconnect_0_1 = RA(triconnect_0_1)
    trimesh.topo[2,0] = Connect(2,0,triconnect_2_0)
    trimesh.topo[1,0] = Connect(1,0,triconnect_1_0)
    trimesh.topo[1,2] = Connect(1,2,triconnect_1_2)
    trimesh.topo[0,1] = Connect(0,1,ra_triconnect_0_1)
    trimesh.fns[0][:bmark] = tribmark

    # 2D triangular not filled
    trimesh_ = Mesh{2, 2, Int, Float64}(trivertices)
    ra_triconnect_0_1 = RA(triconnect_0_1)
    trimesh_.topo[2,0] = Connect(2,0,triconnect_2_0)
    trimesh_.topo[0,1] = Connect(0,1,ra_triconnect_0_1)
    trimesh.fns[0][:bmark] = tribmark

    # its dual voronoi mesh
    vormesh = Mesh{2, 2, Int, Float64}(vorvertices)
    vormesh.topo[2,2] = Connect(2,2,RA(vorconnect_2_2))
    vormesh.topo[1,0] = Connect(1,0,vorconnect_1_0)
    vormesh.fns[0][:bmark] = vorbmark
    trimesh.fns[0][:bmark] = tribmark

    return (mesh11, mesh11_, mesh12, trimesh, trimesh_, vormesh)
end

end #module
