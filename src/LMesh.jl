module LMesh
# This module implements a mesh after Logg (2012)
# http://arxiv.org/pdf/1205.3081.pdf and will try to be general enough
# that structured grids can also be efficiently represented.  The C++
# implementation of it is here:
# https://bitbucket.org/fenics-project/dolfin/src/2ee15a85b7256b6e678f61d44eab7bcbe48c1937/dolfin/mesh?at=master

# Logg-Mesh summary and key concepts (numbering as in his paper):
#
# 2.1) Mesh entity (ME) (d,i), where d=0..D is its topological dimension
#      (e.g. 1 for an edge) and i is an index to uniquely identify it.
#      Names for some topological dimensions: d=0 -> vertex, d=1 ->
#      edge, d=2 -> face, d=D-1 -> facet, d=D -> cell
#      -> See type Entities
#
# 2.2) Mesh connectivity MC: a map d->d' which gives the connectivites of (d,i)
#      in d'.  The map D->0 completely determines the topology of the mesh.
#      -> See type Connect
#
# 2.3) Mesh function (MF): a MF defined for some dimension d takes a
#      value on each of the ME.  For instance a function returning
#      true for all facets on the boundary and false for all in the
#      interior.  Probably implemented as a lookup table.
#      -> See type MeshFn(s)
#
# 2)   Mesh geometry is defined by the location of some of the ME in
#      some metric space, usually the Vertices.
#      -> See type Coordinates
#
#      Mesh topology: the set of all mesh connectivites gives the mesh topology.
#      -> See type Topology
#
#      All of above together gives the mesh.
#      -> See type Mesh
#
#  
using Ragged
using Requires

importall Base
export Mesh, Connect, Topology, MeshFns, MeshFn, MeshFnContainer, Coordinates, Entities, Entity, # main data types
       checkmesh,      # consistency check of mesh
       Cells, Edges, Facets, Vertices, entities,  # convenience constructors for Entities
       tdims, tdim,  # topological dimension(s)
       sdims, # spatial dimensions
       con, con0, # get connectivity of Entities
       meshfns,   # get mesh functions
       coords,      # get vertex coordinates
       incident, incident0,  # get incident relation of Entities
       getmesh,    # get underlying mesh of Entities
       sizes,      # number of each entity in a mesh
       ids,        # entity ids
       midpoints   # midpoints of mesh entities

# include submodules --> at the end of the module

###
# Connectivity and Topology
###

# Logg's class MeshConnectivity
type Connect{I<:Integer, Ar<:Union(Matrix, RaggedArray)} #<: AbstractArray{I,2}
    # connectivity mapping: tdim1->tdim2
    # (Maybe tdim1 and tdim2 could be part of the datatype for dispatch?)
    tdim1::Int
    tdim2::Int
    # connectivity either a Matrix or a RaggedArray (the former is
    # probably a bit faster)
    c::Ar
    function Connect(tdim1, tdim2, c)
        @assert eltype(c)==I
        new(tdim1, tdim2, c)
    end
end
Connect{I<:Integer}(tdim1::Integer,tdim2::Integer,ra::Union(Matrix{I},RaggedArray{I})) =
    Connect{I,typeof(ra)}(tdim1,tdim2,ra)
Connect{T<:Integer}(I::Type{T},tdim1::Integer,tdim2::Integer) = Connect{I,Matrix{I}}(tdim1,tdim2, zeros(I,0,0))

size(con::Connect, i...) = size(con.c, i...)
getindex(con::Connect, i...) = getindex(con.c, i...)
setindex!(con::Connect, newvals, i::Integer...) = setindex!(con.c, newvals, i...)
show(io::IO, con::Connect) = (println(io, "Mesh connectivity array of for map between t-dims $(con.tdim1) -> $(con.tdim2):"); show(io, con.c))
ndims(con::Connect) = 2
tdims(con::Connect) = (con.tdim1, con.tdim2)
isempty(con::Connect) = isempty(con.c)
maximum(con::Connect) = maximum(con.c)
minimum(con::Connect) = minimum(con.c)
==(c1::Connect, c2::Connect) = (c1.tdim1==c2.tdim1 && c1.tdim2==c2.tdim2 && c1.c==c2.c)
eltype{I}(c1::Connect{I}) = I
eltype{C<:Connect}(c1::Type{C}) = c1.parameters[1]

# Container for all the Connect{I}, cf. Logg's class MeshTopology
immutable Topology{TDIMS,I<:Integer}
    t::Matrix{Connect{I}}
    function Topology(t::Matrix)
        if size(t)!=(TDIMS+1,TDIMS+1)
            error("Topological dimension TDIMS=$TDIMS does not correspond to array size =$(size(t))")
        end
        new(t)
    end
end
Topology{C<:Connect}(TDIMS, t::Matrix{C}) = Topology{TDIMS,eltype(C)}(t)
function Topology{I<:Integer}(TDIMS,::Type{I}) # make an empty one
    t = Array(Connect{I}, TDIMS+1, TDIMS+1);
    for i=0:TDIMS
        for j=0:TDIMS
            t[i+1,j+1] = Connect(I,i,j)
        end
    end
    Topology(TDIMS, t)
end
function Topology{C<:Connect}(TDIMS, cons::Vector{C})
    # make one from a list of Connect by putting them into their right place
    I = eltype(cons[1])
    topo = Topology(TDIMS,I) # make an empty one
    for con in cons
        @assert I==eltype(con)
        topo[tdims(con)...] = con
    end
    topo
end

# index is 0-based as dimensions start at zero
size(topo::Topology, i...) = map(x->x-1, size(topo.t, i...))
ndims(topo::Topology) = 2
isempty(topo::Topology) = map(isempty, topo.t)
getindex(topo::Topology, i...) = getindex(topo.t, map(x->x+1, i)...)
setindex!(topo::Topology, newvals, i...) = setindex!(topo.t, newvals, map(x->x+1, i)...)
function show{TDIMS}(io::IO, topo::Topology{TDIMS}) 
    println(io, "Array of mesh connectivities of topological dimension = $TDIMS")
    show(io, topo.t)
end

###
# Mesh
###

abstract MeshOrGrid 
abstract AGrid <: MeshOrGrid  # structured grid 
abstract AMesh <: MeshOrGrid  # unstructured grid (mesh)

typealias Coordinates{R} Array{R,2} # an array to hold coordinates of vertices

typealias MeshFn Vector
typealias MeshFns Dict{Symbol,MeshFn} # Dict to hold mesh-functions

immutable MeshFnContainer{TDIMS}
    # Container for MeshFn which allows indexing with tdim.
    # (This is essentially just a zero-index based vector.)
    t::Vector{MeshFns}
    function MeshFnContainer(mfs)
        @assert length(mfs)==TDIMS+1
        new(mfs)
    end
end
MeshFnContainer(TDIMS) = MeshFnContainer{TDIMS}(MeshFns[MeshFns() for t in 1:(TDIMS+1)])
MeshFnContainer(v::Vector{MeshFns}) = MeshFnContainer{length(v)}(v)

# end is 0-based as dimensions start at zero
size(mfc::MeshFnContainer, i...) = map(x->x-1, size(mfc.t, i...))
ndims(mfc::MeshFnContainer) = 2
isempty(mfc::MeshFnContainer) = map(isempty, mfc.t)
getindex(mfc::MeshFnContainer, i...) = getindex(mfc.t, map(x->x+1, i)...)
setindex!(mfc::MeshFnContainer, newvals, i...) = setindex!(mfc.t, newvals, map(x->x+1, i)...)

# Logg's class Mesh:
type Mesh{TDIMS, SDIMS, I<:Integer, R<:Number} <: AMesh
    # TDIMS: topological dimension
    # SDIMS: dimension of space into which mesh is embedded.  sdim>=tdim
    # I index datatype
    # R data type for coordinates

    # Array containing vertex coordinates (cf. Logg's class MeshGeometry)
    coords::Coordinates{R}
    # TODO: make more general to allow for curved boundaries.  Some of
    # that info will be in the finite elements (?) but some should
    # probably be in here.

    # Array of mappings (cf. Logg's class MeshTopology).  The index into this
    # array needs to correpond to (tdim1, tdim2) of its entries.
    topo::Topology{TDIMS,I}

    # Mesh functions go into this dictionary.  The key is a tuple
    # (tdim, identifier).  The functions themselves are a n-vectors
    # which associates something with the entity-id.
    #
    # TODO: this is bad for type inference as MeshFn is not a concrete type!
    fns::MeshFnContainer

    # Dump for anything else a user might want to keep with the mesh
    dump::Dict{Symbol,Any}
    
    # Constructors:
    check_dims() = TDIMS>SDIMS && error("Topological dimension too large: TDIMS>SDIMS")
    function Mesh() 
        check_dims()
        new(Array(R,SDIMS,1), Topology(TDIMS,I), MeshFnContainer(TDIMS), Dict{Symbol,Any}() )
    end
    function Mesh(coords::Coordinates{R})
        check_dims()
        if ndims(coords)==1
            coords = coords.'
        end
        new(coords, Topology(TDIMS,I), MeshFnContainer(TDIMS), Dict{Symbol,Any}() )
    end
    function Mesh(coords::Coordinates{R}, topo::Topology{TDIMS,I}, fns::MeshFnContainer{TDIMS})
        check_dims()
        new(coords, topo, fns, Dict{Symbol,Any}() )
    end
end
Mesh(TDIMS, SDIMS) = Mesh{TDIMS,SDIMS,Int,Float64}()
Mesh{R}(TDIMS, SDIMS, coords::Coordinates{R}) = Mesh{TDIMS,SDIMS,Int,R}(coords)
function Mesh{TDIMS,I,R}(TDIMS_, SDIMS, coords::Coordinates{R}, topo::Topology{TDIMS,I}, fns::MeshFnContainer{TDIMS})
    if TDIMS!=TDIMS_ 
        error("TDIMS are messed up")
    end
    Mesh{TDIMS_,SDIMS,I,R}(coords, topo, fns)
end

show{TDIMS,SDIMS}(io::IO, me::Mesh{TDIMS,SDIMS}) = 
    print(io, "A ($TDIMS,$SDIMS)-mesh (i.e. with $TDIMS topological and $SDIMS spatial dimensions)")
ndims{TDIMS,SDIMS}(mesh::Mesh{TDIMS,SDIMS}) = (TDIMS,SDIMS)
tdims{TDIMS}(mesh::Mesh{TDIMS}) = TDIMS
sdims{TDIMS,SDIMS}(mesh::Mesh{TDIMS,SDIMS}) = SDIMS
eltype{TDIMS, SDIMS, I, R}(mesh::Mesh{TDIMS, SDIMS, I, R}) = (I, R)

# note, this size is used when indexing, probably should be +1 though!
size(mesh::Mesh, a) = tdims(mesh) # +1
size(mesh::Mesh) = (tdims(mesh), tdims(mesh))


# used to compute "end" for last index.
function Base.trailingsize(mesh::Mesh, n)
    s = 1
    for i=n:tdims(mesh)
        s *= size(mesh,i)
    end
    return s
end

function sizes(mesh::Mesh, tdim)
    # Returns number of entites in dimension tdim.  If no connectivity
    # of tdim has been calculated return -1

    # try coords
    if tdim==0
        return size(mesh.coords,2)
    end
    # try maps tdim-->other
    for ii=0:size(mesh.topo,1)
        if !isempty(mesh.topo[tdim, ii])
            return size(mesh.topo[tdim,ii].c,2)
        end
    end
    # try maps other-->tdim
    for ii=0:size(mesh.topo,2)
        if !isempty(mesh.topo[ii, tdim])
            return maximum(mesh.topo[ii, tdim])
        end
    end
    #error("None of mesh.topo corresponding to tdim=$tdim is initialized.")
    # maybe we want to have this fixed?
    return -1 # not calcualted yet
end
function sizes(mesh::Mesh)
    out = Int[]
    for tdim =0:ndims(mesh)[1]
        push!(out, sizes(mesh,tdim))
    end
    return tuple(out...)
end

# Access to Mesh internals
meshfns(m::Mesh) = m.fns
coords(m::Mesh) = m.coords
coords(m::Mesh, sdim) = squeeze(m.coords[sdim,:],1)
# function coords{TDIMS, SDIMS, I<:Integer, R<:Number}(m::Mesh{TDIMS, SDIMS, I, R}, sdims::Int) 
#     # Expands the coordinate matrix with zeros to sdims.
#     # THIS CAN BE SLOW!
#     if sdims==SDIMS
#         return coords(m)
#     else
#         # expands the coordinate matrix with zeros to sdims
#         out = zeros(R, sdims, size(m.coords,2))
#         for i=1:size(m.coords,2)
#             out[1:SDIMS,i] = m.coords[:,i]
#         end
#         return out
#     end
# end

# indexing gets connectivity arrays
endof(m::Mesh) = error("Cannot use 'end' when single indexing a Mesh")
getindex(m::Mesh, i::Integer) = m.topo[i,0]
getindex(m::Mesh, i...) = m.topo[i...]

# consistency checks (maybe these could go to AMesh instead)
function _check_dims{TDIMS,SDIMS}(m::Mesh{TDIMS,SDIMS})
    @assert TDIMS<=SDIMS 
end
function _check_coords(m::Mesh)
    @assert size(coords(m),2)==length(Vertices(m)) #Mesh coordinates are wrong: $(size(coords(m)))
end
function _check_topology(m::Mesh)
    topo = m.topo
    @assert size(topo,1) == size(topo,2) # "Topology array is not square, size(topo)=$(size(topo))" 
    for i=0:size(topo,1)
        for j=0:size(topo,2)
            con = topo[i,j]
            @assert tdims(con)==(i,j)  #"Topology array entry not consistent with tdims"
            ~isempty(m[i,j]) && @assert size(con,2) == sizes(m,i) # "Topology array entry of wrong size"
        end
    end
end
function checkmesh(m::Mesh)
    # checks mesh integrity, returns mesh if all ok
    _check_dims(m)
    _check_coords(m)
    _check_topology(m)
    return m
end

###
# Mesh Entities
###

# Holds a bunch of entities with topological dimension TDIM: this will
# be returned when iterating over a AEntities
abstract AEntities{TDIM}
immutable Entities{TDIM, I<:Integer} <: AEntities{TDIM}  
    mesh::Mesh
    ids::AbstractVector{I}    # id of the entities
end
# same as above but for just one Entity:
immutable Entity{TDIM, I<:Integer} <: AEntities{TDIM}  
    mesh::Mesh
    ids::I    # id of the entity
end
function Entities(TDIM, mesh::Mesh)
    sz=sizes(mesh, TDIM)
    if sz==-1
        error("No connectivities available for entities of tdim=$TDIM")
    end
    Entities{TDIM, eltype(mesh)[1]}(mesh, 1:sz)
end
Entities(TDIM, mesh::Mesh, ids) = (ent=Entities(TDIM, mesh); ent[ids])
# convenience constructors:  # TODO in 0.4 make these type-aliases
Vertices(mesh) = Entities(0, mesh)
Edges(mesh) = Entities(1, mesh)
Facets{TDIMS}(mesh::Mesh{TDIMS}) = Entities(TDIMS-1, mesh)
Cells{TDIMS}(mesh::Mesh{TDIMS}) = Entities(TDIMS, mesh)
entities(mesh::Mesh) = (Vertices(mesh),Edges(mesh),Facets(mesh),Cells(mesh))

getmesh(ent::AEntities) = ent.mesh
tdim{TDIM}(ent::AEntities{TDIM}) = TDIM
length(ent::AEntities) = length(ent.ids)
endof(ent::AEntities) = endof(ent.ids)
ids(ent::AEntities) = ent.ids

meshfns{I}(e::Entities{I}) = meshfns(getmesh(e))[I]
meshfns{I}(e::Entity{I}, s::Symbol) = meshfns(getmesh(e))[I][s][ids(e)]

# access to mesh characteristics
eltype(ent::AEntities) = eltype(getmesh(ent))
ndims(ent::AEntities) = ndims(getmesh(ent))
tdims(ent::AEntities) = tdims(getmesh(ent))
sdims(ent::AEntities) = sdims(getmesh(ent))

show{TDIM}(io::IO, ent::Entities{TDIM}) = (print(io, "Entities with TDIM=$TDIM of a $(ndims(ent.mesh))-mesh. ids: ");
                                           show(io, ent.ids); print("\n"))
show{TDIM}(io::IO, ent::Entity{TDIM}) = (print(io, "Entity with TDIM=$TDIM of a $(ndims(ent.mesh))-mesh. ids: ");
                                         show(io, ent.ids); print("\n"))
# getindex
getindex{TDIM,I}(ents::Entities{TDIM,I}, ii::Integer) = Entity{TDIM,I}(ents.mesh, ents.ids[ii])
getindex{TDIM,I}(ents::Entities{TDIM,I}, ii) = Entities{TDIM,I}(ents.mesh, ents.ids[ii])


## functions to get info out of Entities

# connectivity TDIM1->tdim2
con{TDIM1}(ents::AEntities{TDIM1}, tdim2) = getmesh(ents)[TDIM1, tdim2][:,ents.ids]
# connectivity Entity->Vertices
con0(ents::AEntities) = con(ents::AEntities,0)

# Coordinates of associated vertices.  sdims_ expands the coordinate
# vectors to sdim with zeros.
function coords{TDIM}(ents::AEntities{TDIM}, sdims_...) 
    m = getmesh(ents)
    R = eltype(m)[2]
    if TDIM==0  # if the Entity is a vertex, return coordinates of itself
        return coords(m,sdims_...)[:,ents.ids]
    else # otherwise of the connected vertices
        length(ents)>1 && error("coords of more than one mesh entity only works for vertices")
        co = coords(m)[:,con(ents, 0)]
        SDIMS = sdims(m)
        if length(sdims_)==0 || sdims_==SDIMS
            return co
        else
            sdims_ = sdims_[1]
            out = zeros(R, sdims_, size(co,2))
            for i=1:size(co,2)
                out[1:SDIMS,i] = co[:,i]
            end
            return out
        end
    end
end

function incident(ent::Entity, tdim2)
    """
    Entities of tdim==tdim2 incident on ent
    """
    ids = con(ent, tdim2) # tdim1->tdim2
    return Entities(tdim2, ent.mesh, ids)
end
incident0(ent::Entity) = incident(ent, 0)

##################################################
## Iterators
# Logg:
# "Mesh iterators can be used to iterate either over the global set of
# mesh entities of a given topological dimension, or over the locally
# incident entities of any given mesh entity. Two alternative
# interfaces are provided; the general interface MeshEntityIterator
# for iteration over entities of some given topological dimension d,
# and the specialised mesh iterators VertexIterator, EdgeIterator,
# FaceIterator, FacetIterator and CellIterator for iteration over
# named entities. Iteration over mesh entities may be nested at
# arbitrary depth and the connectivity (incidence relations) required
# for any given iteration is automatically computed (at the first
# occurrence) by the algorithms presented in the previous section."

# We can write:
# for me in Entities(mesh, tdim)  # me is a mesh entity
#     con(me,other_tdim) # connectivity to another tdim
#     con0(me)            # connectivity to vertices
#     coords(me)            # coords of all vertices in me
#     meshfns(me,key)     # = mesh.fns[(tdim,key)]
# end

# Iterator over mesh entities
start(ents::AEntities) = 1
next(ents::AEntities, state::Int) = (ents[state], state+1)
done(ents::AEntities, state::Int) = length(ents)<state

################################################
## Processing functions
#
# Todo: implement storage

function midpoints(ents::AEntities)
    # get midpoints of entities
    mp = Array(eltype(ents)[2], sdims(ents), length(ents))
    for (i,e) in enumerate(ents)
        mp[:,i] = mean(coords(e),2)
    end
    return mp
end
        


##################
# # This is unfinished.  Note that unlike Logg's claim, the functions
# # below work in 3D only for tetrahedrons!

# ######################################################
# # Connectivty builiding algorithms of Logg's section 4.
# #

# #
# # - the none-! version will return a Array or RaggedArray of the right size
# # - the ! version will populate it and insert it into the mesh.topo
# #
# # - calc_connect! applies the build, transpose and inversect algorithms to 
# #   make a tdim1->tdim2 map.  Only this function will be called by user.
# #
# # Note: there is a lot of code duplication here!!!

# #####
# # for each type of entity with tdim>1 we need to specify a function as
# # Eq.3 in Logg.  Maybe it would make sense to have that in 
# #####
# # 2D: can do all polygons (assuming that the vertices are ordered)
# function local_tdim2tdim0(ent::Entity{2})
#     two2zero = con0(ent)
#     len=length(two2zero)
#     out = Array((Int, Int), len)
#     for i=1:len-1
#         for j=2:len
#             out[i] = two2zero[i,j]
#         end
#     end
#     out[end] = two2zero[end,1]
#     return out
# end

# # 3D: this gets more complicated as there is more than one way to
# #     connect more than four vertices.  (Maybe then it would make
# #     sense to only accept meshes which specify this extra info
# #     already in the 3->2 mapping? And give the mesh-file
# #     reader function the task to produce that mapping already.)

# # tetrahedron: could do that:

# # 

# function make_con(mesh, tdim1, tdim2, nincident, nents)
#     # returns a empty Con of the right size and type
#     tdims,sdims,I,R = mesh_dtype(mesh)
#     if maximum(nincident)==minimum(nincident)
#         Connect(tdim1,tdim2, zeros(I, minimum(nincident), nents))
#     else
#         Connect(tdim1,tdim2, 0*raggedArray.RaggedArray(I, nincident))
#     end
# end
# function index(connect::Connect, vi, kk)
#     # used in build!: returns the id of the right, already constructed tdim-entity
#     vi = sort([vi...])
#     for i=1:kk
#         if all(sort(connect[:,i])==vi)
#             return i
#         end
#     end
#     error("Could not find index: build! alogrithm must be faulty.")
# end


# function _build{TDIMS}(mesh::Mesh{TDIMS}, tdim)
#     # calculates TDIMS -> tdim and tdim -> 0 from TDIMS->0 and
#     # TDIMS->TDIMS provided 0<tdim<TDIMS
#     #
#     # This function initializes the arrays
#     if !(0<tdim<TDIMS)
#         error("Need (0<tdim<TDIMS)")
#     end

#     n_Dd = sizes(mesh, TDIMS)  # size(TDIMS->tdim, 2)  this may fail... 
#     nincident_Dd = zeros(Int, nents1)  # sizes of (TDIMS->tdim, 1)

#     n_d0 = sizes(mesh, tdim)  # length(tdim->0)  this may fail... 
#     nincident_d0 = zeros(Int, nents2) # sizes of (tdim->0, 1)
    
#     kk = 1
#     for Di in Cells(mesh)
#         Vi = local_tdim2tdim0(Di)
#         doneVi = zeros(Bool, length(Vi))
#         nincident_Dd[Di.ids] = length(Vi)  # this is easy
#         Djs = incident(TDIMS, Di)

#         # first go over Dj with i>j, i.e. the ones where we don't create new tdim->0
#         for (nn,Dj) in enumerate(Djs)  
#             if !(Di.ids>Dj.ids)
#                 continue
#             end
#             Vj = local_tdim2tdim0(Dj)
#             for (uu,vi) in enumerate(Vi)
#                 if vi in Vj  # this means there is already an entry in the tdim->0 table
#                     doneVi[uu] = true
#                 end
#             end
#         end
#         # any vi which are not done we need to create an entry in the tdim->0 map
#         for vi in Vi[!doneVi]
#             nincident_d0[kk] = length(vi)
#             kk += 1
#         end
#     end # for Di in Cells(mesh)
#     return  make_con(mesh, TDIMS, tdim, nincident_Dd, n_Dd),  make_con(mesh, tdim, 0, nincident_d0, n_d0)
# end

# function _build!{TDIMS}(mesh::Mesh{TDIMS}, tdim)
#     # calculates TDIMS -> tdim and tdim -> 0 from TDIMS->0 and
#     # TDIMS->TDIMS provided 0<tdim<TDIMS
#     #
#     # This function fills the arrays
#     mesh.topo[TDIMS,tdim], mesh.topo[tdim, 0] =  _build(mesh, tdim)
#     connect_Dd = mesh.topo[TDIMS,tdim]
#     connect_d0 = mesh.topo[tdim, 0]
#     inds = ones(Int,size(connect_Dd.c,2))

#     kk = 1
#     for Di in Cells(mesh)
#         Vi = local_tdim2tdim0(Di)
#         doneVi = zeros(Bool, length(Vi))
#         ind_Dd = 1
#         Djs = incident(TDIMS, Di)

#         # first go over Dj with i>j, i.e. the ones where we don't create new tdim->0
#         for (nn,Dj) in enumerate(Djs)  
#             if !(Di.ids>Dj.ids)
#                 continue
#             end
#             Vj = local_tdim2tdim0(Dj)
#             for (uu,vi) in enumerate(Vi)
#                 if doneVi[uu] # only do each vi once
#                     continue
#                 end
#                 if vi in Vj  # this means there is already an entry in the tdim->0 table
#                     l = index(connect_d0, vi, kk) # find tdim-entity id
#                     connect_Dd[ind_Dd, Di.ids] = l
#                     ind_Dd += 1
#                     doneVi[uu] = true
#                 end
#             end
#         end
#         # any vi which are not done we need to create an entry in the tdim->0 map
#         for vi in Vi[!doneVi]
#             connect_d0[:, kk] = [vi...]
#             connect_Dd[ind_Dd, Di.ids] = kk
#             ind_Dd += 1
#             kk += 1
#         end
#     end # for Di in Cells(mesh)
    
#     # This is Logg's faulty algorithm:
#     # for Di in Cells(mesh)
#     #     Vi = local_tdim2tdim0(Di) # this needs to be defined for each type of mesh:
#     #     for Dj in incident(TDIMS, Di) # for j<i:
#     #         if !(Dj.ids<Di.ids)
#     #             continue
#     #         end
#     #         Vj = local_tdim2tdim0(Dj)
#     #         for vi in Vi
#     #             if vi in Vj
#     #                 to finish:  l = find(vi.==Vj) # find entity id in already created enitites 
#     #                 connect_Dd[inds[Di.ids], Di.ids] = l
#     #             else
#     #                 connect_Dd[inds[Di.ids], Di.ids] = kk
#     #                 connect_d0[:, kk] = [vi...]
#     #                 kk += 1
#     #             end
#     #             inds[Di.ids] += 1
#     #         end
#     #     end
#     # end
# end

# function _transpose(mesh::Mesh, tdim1, tdim2)
#     # calculates connectivity tdim1->tdim2 from tdim2->tdim1 for tdim1<tdim2:
#     # Returns an allocated connectivity array of the right size.

#     if tdim1>=tdim2
#         error("Need tdim1<tdim2")
#     end
#     # first figure out the size of the map and allocate it:
#     nents = sizes(mesh, tdim1) # this may fail...
#     nincident = zeros(Int, nents) 
#     for ent2 in Entities(tdim2, mesh)
#         for ent1 in incident(tdim1, ent2)
#             nincident[ent1.ids] += 1
#         end
#     end
#     return make_con(mesh, tdim1, tdim2, nincident, nents)
# end
# function _transpose!(mesh::Mesh, tdim1, tdim2)
#     # calculates connectivity tdim1->tdim2 from tdim2->tdim1 for tdim1<tdim2:
#     # Inserts it into mesh.topo

#     mesh.topo[tdim1,tdim2] =  _transpose(mesh::Mesh, tdim1, tdim2)
#     connect = mesh.topo[tdim1,tdim2]
#     # now fill the map
#     for ent2 in Entities(tdim2, mesh)
#         ind = 1
#         for ent1 in incident(tdim1, ent2)
#             connect.c[ind,ent1.ids] = ent2.ids
#             ind += 1
#         end
#     end
# end

# function _intersection(mesh::Mesh, tdim1, tdim2, tdim3)
#     # compute tdim1->tdim2 from tdim1->tdim3 and tdim3->tdim2 for tdim1>=tdim1
#     #
#     # Returns an allocated connectivity array of the right size.
#     if !(tdim1>=tdim2)
#         error("Need tdim1>=tdim2")
#     end
#     nents = sizes(mesh, tdim1)
#     nincident = zeros(Int, nents) 
    
#     for ent1 in Entities(tdim1, mesh)
#         i = ent1.ids
#         vertices1 = incident(0, ent1).ids
#         tmp = Int[]
#         for ent3 in incident(tdim3, ent1)
#             k = ent3.ids
#             for ent2 in incident(tdim2, ent2)
#                 j = ent2.ids
#                 vertices2 = incident(0, ent2).ids
#                 if (tdim1==tdim2 && i!=j) || (tdim>tdim1 && issubset(vertices2, vertices1))
#                     if !(j in tmp)
#                         push!(tmp, j)
#                         nincident[i] += 1
#                     end
#                 end
#             end
#         end
#     end
#     return make_con(mesh, tdim1, tdim2, nincident, nents)
# end
# function _intersection!(mesh::Mesh, tdim1, tdim2, tdim3)
#     # compute tdim1->tdim2 from tdim1->tdim3 and tdim3->tdim2 for tdim1>=tdim1
#     #
#     # Fills the connectivity array
#     mesh.topo[tdim1,tdim2] = _intersection(mesh, tdim1, tdim2, tdim3)
#     connect = mesh.topo[tdim1,tdim2]
    
#     for ent1 in Entities(tdim1, mesh)
#         i = ent1.ids
#         vertices1 = incident(0, ent1).ids
#         ind = 1
#         for ent3 in incident(tdim3, ent1)
#             k = ent3.ids
#             for ent2 in incident(tdim2, ent2)
#                 j = ent2.ids
#                 vertices2 = incident(0, ent2).ids
#                 if (tdim1==tdim2 && i!=j) || (tdim>tdim1 && issubset(vertices2, vertices1))
#                     if !(j in connect.c[1:ind-1, i])
#                         connect.c[ind, i] = j
#                         ind += 1
#                     end
#                 end
#             end
#         end
#     end
    
# end

# function calc_connect!{TDIMS}(mesh::Mesh{TDIMS}, tdim1::Integer, tdim2::Integer)
#     # To biuld any map tdim1-tdim2.
#     #
#     # This functions applies _build!, _transpose!, _intersection! and
#     # itself to achive that.
#     if !isdefined(mesh.topo, tdim1, 1)
#         _build!(mesh, tdim1)
#     end
#     if !isdefined(mesh.topo, tdim2, 1)
#         _build!(mesh, tdim2)
#     end
#     if isdefined(mesh.topo, tdim1, tdim2)
#         return
#     end

#     if tdim1<tdim2
#         calc_connect!(mesh, tdim2, tdim1)
#         _transpose!(mesh, tdim1, tdim2)
#     else
#         if tdim1==0 && tdim2==0
#             tdim3 = TDIMS
#         else
#             tdim3 = 0
#         end
#         calc_connect!(mesh, tdim1, tdim3)
#         calc_connect!(mesh, tdim3, tdim2)
#         _intersection!(mesh, tdim1, tdim2, tdim3)
#     end
# end

# # 3) Reading/generating meshes:
# #    - gmsh
# #    - Triangle

# # 4) Mesh ordering
# #    - use http://webpages.charter.net/reinerwt/fem.htm: 
# #    - see glads_matlab/mesh/order_mesh.m


include("submodule.jl")
include("plotting.jl")
end # module

