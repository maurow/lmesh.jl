@require Winston begin
# cannot be a submodule of MeshJu because then Winston gets loaded on
# import and I want to avoid that (takes long, add an additional dependency)
using Winston
import Winston: plot, oplot
export plot

function plot(m::Mesh{1,1})
    edges = Edges(m)
    p=plot()
    for e in edges
        oplot(coords(e)[1,:],[0,0], "-o")
    end
    display(p)
    return p
end

function plot(m::Mesh{1,2})
    edges = Edges(m)
    p=plot()
    for e in edges
        oplot(coords(e)[1,:],coords(e)[2,:], "-")
    end
    display(p)
    return p
end

function plot(m::Mesh{2,2};symb=".-")
    edges = Edges(m)
    vertices = Vertices(m)
    p=plot()
    for e in edges
        if meshfns(e,:bmark)==0
            oplot(coords(e)[1,:],coords(e)[2,:], symb)
        elseif mod(meshfns(e,:bmark),2)==0
            oplot(coords(e)[1,:],coords(e)[2,:], "r"*symb)
        else
            oplot(coords(e)[1,:],coords(e)[2,:], "g"*symb)
        end
    end
    for v in vertices
        if meshfns(v,:bmark)==0
            oplot(coords(v)[1,:],coords(v)[2,:], "o")
        elseif mod(meshfns(v,:bmark),2)==0
            oplot(coords(v)[1,:],coords(v)[2,:], "ro")
        else
            oplot(coords(v)[1,:],coords(v)[2,:], "go")
        end
    end
    println("Dirichlet in green, Neumann in red")
    display(p)
    return p
end

end # end require
