# provides simplex meshes in 1D and 2D in simple domains (i.e. a line or square or cube)


# 1D boxmesh
function make_boxmesh(n, xrange; bmark=ones(Int,2), rand_fac=0)
    #  mesh = make_1D_boxmesh(a, b, n; bmark=ones(Int,2), rand_fac=0)
    #
    # Makes a 1D Mesh.
    #
    # 2 or 3 argument call: 
    #  - n: number of points
    #  - 
    #  - rand_fac: adds a random offset to each node coordinate

    if rand_fac>=0.5
        error("rand_fac needs be smaller than 0.5")
    end
    if diff(xrange)[1]<=0
        error("xrange must be strictly increasing")
    end


    vertices = linspace(xrange[1],xrange[2],n)' # convert into 2D array
    n_vertices = n
    n_cells = n-1
    n_facets = n-1

    # randomize:
    dx = vertices[2]-vertices[1]
    vertices = vertices + rand(1,n)*rand_fac*dx;   

    connect_1_0 = Connect(1,0,[[1:n-1]', [2:n]'])
    tmp = Array{Int,1}[[1]]
    append!(tmp, Array{Int,1}[[i,i+1] for i=1:n_cells-1])
    push!(tmp, [n_cells])
    connect_0_1 = Connect(0,1, RaggedArray(tmp))

    tmp = Array{Int,1}[[2]]
    append!(tmp, Array{Int,1}[[i-1,i+1] for i=2:n_vertices-1])
    push!(tmp, [n_vertices-1])
    connect_0_0 = Connect(0,0, RaggedArray(tmp))
    topo = Topology(1, [connect_0_1, connect_1_0, connect_0_0])

    bmark_ = zeros(Int,n_vertices)
    bmark_[1] = bmark[1]
    bmark_[end] = bmark[2]
    mfns = MeshFnContainer(1)
    mfns[0][:bmark] = bmark_
    mfns[1][:bmark] = zeros(Int,n_facets)

    checkmesh(Mesh(1,1,vertices, topo, mfns))
end

# 2D boxmesh
function make_boxmesh(nx, ny, xrange, yrange; bmark_vertex=ones(Int,4), bmark_facet=ones(Int,4), 
                      meshtype=[:simlex,:quad][1], diag=[:bltr, :tlbr][1], order_mesh=false)
    # make_boxmesh(nx, ny, xrange, yrange, bmark, bmark_facet; order_mesh=false)
    #
    # Creates a structured triangular or quad mesh on a rectangle.
    # 
    # diag = [:bltr, :tlbr] -- whether diagonals go bottom-left to top right or other way round
    #
    # bmark_vertex -- 4-element vector with boundary marks of the four corners, from (0,0) anti-clockwise, defaults to 1
    # bmark_facet -- 4-element vector with boundary marks for the edges, from (0,0) anti-clockwise, defaults to 1

    if order_mesh
        error("Mesh ordering not implemented")
    end
    if diff(xrange)[1]<=0 || diff(yrange)[1]<=0 
        error("xrange and yrange must be strictly increasing")
    end
    if meshtype==:simlex
        simplex = true
    elseif  meshtype==:quad
        simplex = false
    else
        error("meshtype option needs to be :simplex of :quad")
    end
    if diag==:bltr
        diag1=true
    elseif diag==:tlbr
        diag1=false
    else
        error("diag option needs to be :bltr or :tlbr")
    end

    n_vertices = nx*ny
    if simplex
        n_cells = (nx-1)*(ny-1)*2
        n_facets = (nx-1)*ny + (ny-1)*nx + iround(n_cells/2)
        connect = Array(Int,3, n_cells)
    else
        n_cells = (nx-1)*(ny-1)
        n_facets = (nx-1)*ny + (ny-1)*nx
        connect = Array(Int,4, n_cells)
    end
    connect_facet = Array(Int,2, n_facets)
    
    ## construct the node coordinates
    vertices =  zeros(Float64, 2, n_vertices)
    dx = (diff(xrange)/(nx-1))[1]
    dy = (diff(yrange)/(ny-1))[1]
    (xs,ys) = linspace(xrange[1],xrange[2], nx), linspace(yrange[1],yrange[2], ny)
    for i=1:length(xs)
        for j=1:length(ys)
            vertices[:,length(ys)*(i-1)+j] = [xs[i],ys[j]]
        end
    end

    ## construct 2->0 connectivity
    n = 1 # cell number
    e = 1 # edge number
    for col=1:nx-1  # loop over columns, make quad element & split them up if simplex
        # c1+1 --- c2+1
        #  |        |
        #  |        |
        # c1   --- c2
        c1 = (col-1)*ny+1
        c2 = (col)*ny+1
        for row=1:ny-1 # rows of quads
            # facets
            connect_facet[:,e] = [c1,c1+1]
            e += 1
            connect_facet[:,e] = [c1,c2]
            e += 1
            if simplex
                if diag1 #bltr diagonal
                    connect[:,n] = [c1, c1+1, c2+1]
                    n+=1
                    connect[:,n] = [c1, c2, c2+1]
                    n+=1
                    # facets
                    connect_facet[:,e] = [c1,c2+1]
                    e += 1
                else
                    connect[:,n] = [c1, c1+1, c2]
                    n+=1
                    connect[:,n] = [c2, c2+1, c1+1]
                    n+=1
                    # facets
                    connect_facet[:,e] = [c1+1,c2]
                    e += 1
                end
            else # quad
                connect[:,n] = [c1, c2, c2+1, c1+1]
                n+=1
            end
            # facets:
            # check whether we're at the top edge of the domain
            if row==ny-1
                connect_facet[:,e] = [c1+1,c2+1]
                e += 1
            end                
            # check whether we're at the right edge of the domain
            if col==nx-1
                connect_facet[:,e] = [c2,c2+1]
                e += 1
            end                
            c1 += 1
            c2 += 1
        end
    end

    ## boundary marks
    small = min(dx,dy)/100
    bmark = zeros(Int,n_vertices)
    # vertices made along edges
    sq1(x) = squeeze(x,1)
    bmark[sq1(vertices[1,:].<xrange[1]+small)] = bmark_facet[4]
    bmark[sq1(vertices[1,:].>xrange[2]-small)] = bmark_facet[2]
    bmark[sq1(vertices[2,:].<yrange[1]+small)] = bmark_facet[1]
    bmark[sq1(vertices[2,:].>yrange[2]-small)] = bmark_facet[3]
    # four corners
    bmark[1] = bmark_vertex[1]
    bmark[ny] = bmark_vertex[2]
    bmark[nx*ny] = bmark_vertex[3]
    bmark[nx*ny-ny+1] = bmark_vertex[4]
    # facets
    on_boundary = (bmark[sq1(connect_facet[1,:])].>0) & (bmark[sq1(connect_facet[2,:])].>0)
    facet_midpoints = (vertices[:, sq1(connect_facet[1,:])] + vertices[:, sq1(connect_facet[2,:])])/2
    bmark_facet_ = zeros(Int,n_facets)

    bm = sq1(facet_midpoints[2,:].<yrange[1]+small) & on_boundary
    bmark_facet_[bm] = bmark_facet[1]
    bm = sq1(facet_midpoints[2,:].>yrange[2]-small) & on_boundary
    bmark_facet_[bm] = bmark_facet[3]
    bm = sq1(facet_midpoints[1,:].<xrange[1]+small) & on_boundary
    bmark_facet_[bm] = bmark_facet[4]
    bm = sq1(facet_midpoints[1,:].>xrange[2]-small) & on_boundary
    bmark_facet_[bm] = bmark_facet[2]

    ### ordering: todo but symrcm does not exist in Julia
#-> see https://github.com/dmbates/Metis.jl
    # if order_mesh
    #     ## order mesh: takes a long time!
    #     ce1 = connect_facet[1,:]
    #     ce2 = connect_facet[2,:]
    #     row = zeros(n_vertices*6)
    #     col = zeros(n_vertices*6)
    #     val = 1
    #     counter = 1
    #     for jj = 1:n_vertices
    #         tvertices1 = find(ce1==jj)
    #         tvertices2 = find(ce2==jj);    
    #         nadd = length(tvertices1)+length(tvertices2)
    #         row[counter:counter+nadd-1] = jj
    #         col[counter:counter+nadd-1] = [ce2[tvertices1]; ce1[tvertices2]]
    #         counter = counter + nadd
    #     end
    #     row = row[1:counter-1]
    #     col = col[1:counter-1]
    #     neigh = sparse(row, col, val, n_vertices, n_vertices)
    #     # permutation to apply:

    #     todo: this does not exist in Julia:
    #     per = symrcm(neigh)
    #     # inverse permutation http://blogs.mathworks.com/loren/2007/08/21/reversal-of-a-sort/
    #     tmp = 1:n_vertices
    #     inv_per[per] = tmp

    #     # do permutation
    #     vertices = vertices[per,:]
    #     bmark = bmark[per]
    #     # cells
    #     connect = inv_per[connect]
    #     connect_facet = inv_per[connect_facet]
    # end

    # make Mesh
    connect_2_0 = Connect(2, 0, connect)
    connect_1_0 = Connect(1, 0, connect_facet)
    topo = Topology(2, [connect_1_0, connect_2_0])

    mfns = MeshFnContainer(2)
    mfns[0][:bmark] = bmark
    mfns[1][:bmark] = bmark_facet_

    checkmesh(Mesh(2, 2, vertices, topo, mfns))
end

# 3D boxmesh
function make_boxmesh(nx, ny, nz, xrange, yrange, zrange; bmark_vertex=ones(Int,8), bmark_facet=ones(Int,6),
                      meshtype=[:simlex,:quad][1], order_mesh=false)
#       6
#    +------+   
#    |\     |\  
#    | +------+ 
#   2| | 5  | | 
#    +-|----+ |4 
#     \|  3  \| 
#      +------+ 
#         1
# (ASCII art box adapted from Richard Kirk)

    if order_mesh 
        error("Mesh ordering not implemented") 
    end 
    if diff(xrange)[1]<=0 || diff(yrange)[1]<=0 || diff(zrange)[1]<=0
         error("xrange, yrange and zrange must be strictly increasing") 
    end
    if meshtype==:simlex
        simplex = true
    elseif  meshtype==:quad
        simplex = false
    else
        error("meshtype option needs to be :simplex of :quad")
    end

    n_vertices = nx*ny*nz
    # number of quad-cells entities:
    n_quads = (nx-1)*(ny-1)*(nz-1)
    n_facet_quads = (nx-0)*(ny-1)*(nz-1) + (nx-1)*(ny-0)*(nz-1) + (nx-1)*(ny-1)*(nz-0)
    n_quad_edges = (nx-0)*(ny-0)*(nz-1) + (nx-1)*(ny-0)*(nz-0) + (nx-0)*(ny-1)*(nz-0)
    
    if simplex
        # number of tet-cells entities:
        n_cells = n_quads*5
        n_facets = n_facet_quads*2 + 4*n_quads
    else
        # number of quad-cells entities:
        n_cells = n_quads
        n_facets = n_facet_quads
    end

    ## construct the node coordinates
    vertices =  zeros(Float64, 3, n_vertices)
    dx = (diff(xrange)/(nx-1))[1]
    dy = (diff(yrange)/(ny-1))[1]
    dz = (diff(zrange)/(nz-1))[1]
    (xs,ys,zs) = linspace(xrange[1],xrange[2], nx), linspace(yrange[1],yrange[2], ny), linspace(zrange[1],zrange[2], nz)
    ind = 1
    for i=1:length(xs)
        for j=1:length(ys)
            for k=1:length(zs)
                vertices[:,ind] = [xs[i],ys[j],zs[k]]
                ind += 1
            end
        end
    end

    ## construct 3->0 connectivity
    # for a quad containing 5 tets: http://www.iue.tuwien.ac.at/phd/wessner/node32.html
    n = 1 # tri el number
    if simplex
        connect = zeros(Int,4, n_cells)
    else
        connect = zeros(Int,8, n_cells)
    end
    for col=1:nx-1 # columns of quads
        for row=1:ny-1 # rows of quads
            # A base of a quad in the bottom layer:
            # d    c
            #  +--+
            #  |  |
            #  +--+
            # a    b
            a = (row-1)*nz + (col-1)*ny*nz + 1
            b = a + ny*nz
            c = b + nz
            d = a + nz
            for lay=1:nz-1 # layers of quads
                # this moves a,b,c,d up some layers
                aa,bb,cc,dd= [a,b,c,d].+(lay-1)
                if simplex
                    # need to "checkerboard" the cube into the two different orientations:
                    if iseven(lay+row+col) 
                        connect[:,n] = [bb,aa,dd,aa+1]
                        n +=1
                        connect[:,n] = [bb,cc,dd,cc+1]
                        n +=1
                        connect[:,n] = [aa+1,bb+1,cc+1,bb]
                        n +=1
                        connect[:,n] = [aa+1,dd+1,cc+1,dd]
                        n +=1
                        connect[:,n] = [aa+1,cc+1,bb,dd]
                        n +=1
                    else
                        connect[:,n] = [aa,bb,cc,bb+1]
                        n +=1
                        connect[:,n] = [aa,dd,cc,dd+1]
                        n +=1
                        connect[:,n] = [bb+1,aa+1,dd+1,aa]
                        n +=1
                        connect[:,n] = [bb+1,cc+1,dd+1,cc]
                        n +=1
                        connect[:,n] = [aa,cc,bb+1,dd+1]
                        n +=1
                    end
                else # quad
                    connect[:,n] = [aa,bb,cc,dd,aa+1,bb+1,cc+1,dd+1]
                    n += 1
                end
            end
        end
    end
    
    # the connect_edge and connect_facet construction is very slow!
    ## construct 2->0 connectivity
    if simplex
        connect_facet_dup = [connect[[1,2,3],:] connect[[1,2,4],:] connect[[1,3,4],:] connect[[2,3,4],:]]
    else
        connect_facet_dup = [connect[[1,2,3,4],:] connect[[1,2,5,6],:] connect[[2,3,6,7],:] connect[[3,4,7,8],:] connect[[4,1,8,5],:] connect[[5,6,7,8],:]]
    end
    connect_facet = unique(sort(connect_facet_dup, 1),2)
    ## construct 1->0 connectivity
    if simplex
        connect_edge_dup = [connect[[1,2],:] connect[[1,3],:] connect[[1,4],:] connect[[2,3],:] connect[[2,4],:] connect[[3,4],:]]
    else
        connect_edge_dup = hcat(connect[[1,2],:], connect[[2,3],:], connect[[3,4],:], connect[[4,1],:], 
                                connect[[1,5],:], connect[[2,6],:], connect[[3,7],:], connect[[4,8],:], 
                                connect[[5,6],:], connect[[6,7],:], connect[[7,8],:], connect[[8,5],:])
    end
    connect_edge = unique(sort(connect_edge_dup, 1),2)


    ## boundary marks
    small = min(dx,dy,dz)/100
    bmark = zeros(Int,n_vertices)
    # vertices made on exterior facets
    sq1(x) = squeeze(x,1)
    bmark[sq1(vertices[1,:].<xrange[1]+small)] = bmark_facet[2]
    bmark[sq1(vertices[1,:].>xrange[2]-small)] = bmark_facet[4]
    bmark[sq1(vertices[2,:].<yrange[1]+small)] = bmark_facet[3]
    bmark[sq1(vertices[2,:].>yrange[2]-small)] = bmark_facet[5]
    bmark[sq1(vertices[3,:].<zrange[1]+small)] = bmark_facet[1]
    bmark[sq1(vertices[3,:].>zrange[2]-small)] = bmark_facet[6]
    # eight corners
    c1,c2,c3,c4 = 1, (nx-1)*ny*nz+1, (nx-1)*ny*nz+1+(ny-1), (ny-1)*nz+1
    bmark[c1] = bmark_vertex[1]
    bmark[c2] = bmark_vertex[2]
    bmark[c3] = bmark_vertex[3]
    bmark[c4] = bmark_vertex[4]
    bmark[c1+nz-1] = bmark_vertex[5]
    bmark[c2+nz-1] = bmark_vertex[6]
    bmark[c3+nz-1] = bmark_vertex[7]
    bmark[c4+nz-1] = bmark_vertex[8]
    # facets
    on_boundary = (bmark[sq1(connect_facet[1,:])].>0) & (bmark[sq1(connect_facet[2,:])].>0) & (bmark[sq1(connect_facet[3,:])].>0)
    facet_midpoints = (  vertices[:, sq1(connect_facet[1,:])] 
                       + vertices[:, sq1(connect_facet[2,:])] 
                       + vertices[:, sq1(connect_facet[3,:])])/3
    bmark_facet_ = zeros(Int,n_facets)

    bm = sq1(facet_midpoints[1,:].>xrange[1]+small) & on_boundary
    bmark_facet_[bm] = bmark_facet[2]
    bm = sq1(facet_midpoints[1,:].>xrange[2]-small) & on_boundary
    bmark_facet_[bm] = bmark_facet[4]

    bm = sq1(facet_midpoints[2,:].<yrange[1]+small) & on_boundary
    bmark_facet_[bm] = bmark_facet[3]
    bm = sq1(facet_midpoints[2,:].>yrange[2]-small) & on_boundary
    bmark_facet_[bm] = bmark_facet[5]

    bm = sq1(facet_midpoints[3,:].<zrange[1]+small) & on_boundary
    bmark_facet_[bm] = bmark_facet[1]
    bm = sq1(facet_midpoints[3,:].>zrange[2]-small) & on_boundary
    bmark_facet_[bm] = bmark_facet[6]

    ## make Mesh
    connect_3_0 = Connect(3, 0, connect)
    connect_2_0 = Connect(2, 0, connect_facet)
    connect_1_0 = Connect(1, 0, connect_edge)
    topo = Topology(3, [connect_2_0, connect_3_0, connect_1_0])

    mfns = MeshFnContainer(3)
    mfns[0][:bmark] = bmark
    mfns[2][:bmark] = bmark_facet_

    checkmesh(Mesh(3, 3, vertices, topo, mfns))
end

