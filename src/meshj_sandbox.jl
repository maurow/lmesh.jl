require("../../Ragged.jl/src/Ragged.jl")
include("../test_meshes/example_meshes.jl")
# do something with them

using Ragged
RA = RaggedArray
using MeshJu
mh = MeshJu

# Connectivity stuff
con = mh.Connect(1,2,Int)
t = [con con; con con];
topo1 = mh.Topology(1, t)

topo = mh.Topology(3, Int)

# triangular mesh with
# 13 vertices
# 28 edges
# 16 cells
nums = [13,28,16]
mesh11, mesh11_, mesh12, trimesh, trimesh_, vormesh = make_meshes()

### entities
vertices = mh.Vertices(mesh11)
v1=vertices[1]
v2=vertices[2]


vertices = mh.Vertices(trimesh)
v1=vertices[1]
v2=vertices[2]
edges = MeshJu.Edges(trimesh)
cells = MeshJu.Cells(trimesh)
v=vertices[1]
e=edges[1]
c=cells[1]

### 1D mesh
mesh1d = make_boxmesh(8, [-1.2, 5.6])

#######
nothing